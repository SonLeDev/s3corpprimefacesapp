package com.s3corp.spring.service;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.swing.JButton;

import org.apache.commons.collections.CollectionUtils;

import com.s3corp.dao.JdbcUtils;
import com.s3corp.hibernate.data.Employee;

@ManagedBean
@ApplicationScoped
public class EmployeeService implements Serializable{
	
	public void register(Employee emp){
		// Acquire session
		JdbcUtils jdbcUtils = new JdbcUtils();
		String sqlInsert = "INSERT INTO s3corp.\"employee\"(\"emp_name\",\"emp_hire_date\",\"emp_salary\") VALUES (?,?,?)";
		
		Object[] params = {emp.getEmployeeName(),emp.getEmployeeHireDate(),emp.getEmployeeSalary()};
		try {
			jdbcUtils.executeUpdate(sqlInsert, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			jdbcUtils.close();
		}
		
		
	}
	public List<Employee> getEmployees() {
		List<Employee> list = new ArrayList<>();
		
		JdbcUtils jdbcUtils = new JdbcUtils();
		String sqlSelect = "SELECT \"emp_id\" as id, \"emp_name\" as name, \"emp_hire_date\" as date, \"emp_salary\" as salary \r\n" + 
				"  FROM S3CORP.\"employee\"\r\n" + 
				"";
		
		ResultSet rs;
		try {
			rs = jdbcUtils.select(sqlSelect, null);
			while(rs.next()) {
				Employee emp = new Employee();
				emp.setEmployeeHireDate(rs.getDate("date"));
				emp.setEmployeeId(rs.getLong("id"));
				emp.setEmployeeName(rs.getString("name"));
				emp.setEmployeeSalary(rs.getDouble("salary"));
				list.add(emp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
		
	}
	
	public Boolean delete(List<Employee> selectedEmployees) {
		
		if(CollectionUtils.isEmpty(selectedEmployees)) return false;
		
		StringBuilder sqlBuilder = new StringBuilder("DELETE FROM s3corp.\"employee\" WHERE \"emp_id\" IN (");
		
		Object[] params = new Long[selectedEmployees.size()];
		
		for(int i = 0 ; i < selectedEmployees.size() ; i ++) {
			params[i] = selectedEmployees.get(i).getEmployeeId();
			sqlBuilder.append("?,");
		}
		sqlBuilder.deleteCharAt(sqlBuilder.length()-1);
		sqlBuilder.append(")");
		
		JdbcUtils jdbcUtils = new JdbcUtils();
		
		return jdbcUtils.executeQuery(sqlBuilder.toString(),params);
	}

}
